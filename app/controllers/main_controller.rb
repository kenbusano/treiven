class MainController < ApplicationController
    def index
      @image_urls = [
        'https://images.pexels.com/photos/18133948/pexels-photo-18133948/free-photo-of-portrait-of-woman-in-traditional-clothing-and-hat.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
        'https://images.pexels.com/photos/26987191/pexels-photo-26987191/free-photo-of-processed-with-vsco-with-m5-preset.jpeg?auto=compress&cs=tinysrgb&w=600&lazy=load'
      ]
    end
end