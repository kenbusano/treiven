Rails.application.routes.draw do
  # get my root route
  root to: "main#index"

  # other pages
  get "about-us", to: "about#index", as: :about

end